import requests
import json
import base64
import time

defaulturl = 'https://test.archive.fhi.mpg.de'
superusername = 'goette'
superpassword = 'eep0oniech5aiy5m'
defaultproject = 'default'

class ACarchive():
    """
    class which handles the communication with the ACarchive
    """
    def __init__(self,_url=defaulturl,_username=superusername,_password=superpassword,_project=defaultproject):
        """
        initialize communication with ACarchive
    
        Parameters
        ----------
        url : string, optional
             url where AC archive is hosted. The default is defaulturl.
        username : TYPE, optional
             valid username of ACarchive The default is superusername.
        password : TYPE, optional
             valid password of ACarchive The default is superpassword.
        project : TYPE, optional
            project to which the class should connect, e.g.  default or catlab 
            The default is defaultproject.

        Returns
        -------
        None.

        """
        self.url = _url
        self.username = _username
        self.password = _password
        self.project = _project
        self.token = None
        self.getToken()

    def makeRequest(self,_endpoint,_payload):
        """
        makes a request to the ACarchive

        Parameters
        ----------
        endpoint : String
            endpointto be used.
        payload : dictionary
            dictionary to be send.

        Returns
        -------
        response_data : dictionary
            jsondictionary with the response

        """
        assert isinstance(_payload, dict)

        code = 403
        counter = 0
        while counter <= 5:
            counter+=1
            try:
                response = requests.get(self.url+_endpoint,data=json.dumps(_payload))
                 
                response_data = json.loads(response.content.decode('utf8'))

                code = response.status_code
                if response_data['success'] == 0 and 'error' in response_data:  
                    print(f"Error meassage: {response_data['error']}") 
                    if response_data['error'] == 'invalid token' and 'token' in _payload :
                       self.getToken() 
                       _payload['token'] = self.token
                    else:
                        return response_data
                            
                elif code != 200:
                    print(f"error code= {code}, retrying")
                    response.raise_for_status()
    
                elif code == 200 and response_data['success'] == 1:
                    return response_data
                    
            except:
                 pass
            time.sleep(0.5)
        return None
            
        
    def getToken(self):
        
        """
        use a username and password to get a token for the AC Archive using the 
        the specified url fromabove
    
        Parameters
        ----------
       
        Returns
        ------------
         token : string, string
         
             token is a string used to authenticate an ACarchive request session
                     
        """
        login_credentials = {"user":self.username,"password":self.password}
        response_data = self.makeRequest('/v1/login',login_credentials)
        
        if response_data: 
            token = response_data['token']
            if token:
                print(f"Token is {token}")
                self.token = token
                return token
            else:
                print("Error requesting Token. Response is None.")
    
    def getFields(self,_entrytype):
        """
        get fields for givenentry type
        
        Parameters
        ----------
        _entrytype : string
            entrytype, either data or sample
     
        Returns
        ------------
         fields : list of strings
             
        """
        assert _entrytype in  ['dNoneata','sample'] 
        payload = {"token":self.token,"type":_entrytype}
        response_data = self.makeRequest('/v1/fields',payload)
        if response_data: 
            if response_data['success'] == 1:
                fields = [response_data['fields'][field]['name'] for field in response_data['fields'] ]
                return fields
        return None
    
    def addLinks(self,_entryid,_links):
        """
        get fields for givenentry type
        
        Parameters
        ----------
        _entryid : string
             id for a sample or data
        _links : list of strings
            list of ids to be linked to _entryid
     
        Returns
        ------------
             
        """
        assert isinstance(_links, list)
        payload = {"token":self.token,"links":_links}
        response_data = self.makeRequest('/v1/addlinks/'+_entryid,payload)
        if response_data: 
            if response_data['success'] == 1:
                return 1
        return None

    def uploadNexusFileBase64(self,_entryID,_filepath,_filename):
        """
        convert a given file in file path to base64 andupload it to AC archive
        to sample withthe given ID
    
        Parameters
        ----------
        entryID : string
            the entry id like S35296
        filepath : string
            the path to the file like 2022_02_08/nx/00002_8161523.hdf
        filename: string
            the name of the file in the AC archive new_name.hdf
            
        Returns
        ------------
         success : integer
         
             0 means no success, 1 means success             
        """
        with open(_filepath,'rb') as file:
            data = base64.b64encode(file.read()).decode('utf8')
        
        payload = {"token":self.token,"filename":_filename,"content":data}
        
        response_data = self.makeRequest('/v1/directupload/'+_entryID,payload)
        if response_data: 
            if response_data['success'] == 1:
                print("Uploaded file")
                return response_data['docid']
        return None 
        
    def downloadFile(self,_entryID,_docID,_filename):
        """
        Downloads a file
        
        Parameters
        ----------
        _entryID : String
            entry id eg. S456 or D545.
        _docid : String
            document id 1234.
        _filename : String
            nameofoutput file

        Returns
        -------
        None.

        """
        _docID = str(_docID)
        payload = {"token":self.token}
        response = requests.get(self.url +'/v1/download/'+_entryID+'/'+_docID,data=json.dumps(payload))
        if response: 
            with open(_filename, "wb") as f:
                f.write(response.content)
                print(f"Downloaded file {_filename}")
                return 1
        else:
            print("Failed Downloading file")
        return None
            


    def createEntry(self,_data,_entrytype='sample'):
        """
        creates a new sample in the AC archive
    
        Parameters
        ----------
        data : json
            dictionary containing the data for the AC archive
            possbile fields are: "Title","Author","Comment","Abstract","Keywords","Document Type","Methods","Elements"
        entrytype string
            either 'sample' or 'data'
            
        Returns
        ------------
         sample Id : string
         
             the Sample ID creatd by the AC archive, like  S35296          
        """
        assert isinstance(_data, dict)
        assert _entrytype in  ['data','sample'] 
        
        payload = {"token":self.token,"type":_entrytype,"project":self.project}
        fields = self.getFields(_entrytype)
        for i,field in enumerate(fields):
            if field in _data:
                payload['f'+str(i)] = _data[field]
        
        response_data = self.makeRequest('/v1/new',payload)
        if response_data: 
            if  response_data['success'] == 1:
                entryID = response_data['id']
                print(f"Created Entry, entryID: {entryID}")
                return entryID  
        return None
    
    def editEntry(self,_entryID,_data):
        """
        edit a given sample in the AC archive
    
        Parameters
        ----------
        _entryID string
            id of entry to bechanged,e.g. S456
        data : json
            dictionary containing the data for the AC archive
            possbile fields are: "Title","Author","Comment","Abstract","Keywords","Document Type","Methods","Elements"

        Returns
        ------------
         
        """
        assert isinstance(_data, dict)
        entrytype = None
        if _entryID[0] == 'S':
            entrytype = 'sample'
        elif _entryID[0] == 'D':
            entrytype = 'data'
        assert entrytype
        payload = {"token":self.token}
        fields = self.getFields(entrytype)
        for i,field in enumerate(fields):
            if field in _data:
                payload['f'+str(i)] = _data[field]
        
        response_data = self.makeRequest('/v1/edit/'+_entryID,payload)
        if response_data: 
            if  response_data['success'] == 1:
                entryID = response_data['id']
                print(f"Edited Entry, entryID: {entryID}")
                return 1
            else:    
                print(f"Failed editing Entry, entryID: {_entryID}")
        return None

    
    def deleteEntry(self,_entryID):
        """

        Parameters
        ----------
        _entryID : string
            ID of entry which should be deleted, e.g. S5432.

        Returns
        -------
        None.

        """
        payload = {"token":self.token}
        response_data = self.makeRequest('/v1/delete/'+_entryID,payload)
        if response_data: 
            if  response_data['success'] == 1:
                print(f"deleted entry {response_data['id']}, wanted to delete {_entryID}")
                return 1
            else:
                print(f"did not delete {_entryID}")
        return  None
        
    def getEntry(self,_entryID):
        """
        get data of entry forgiven entry ID
        Parameters
        ----------
        _entryID : string
            ID of entry which should be deleted, e.g. S5432.

        Returns
        -------
        dat.

        """
        payload = {"token":self.token}
        response_data = self.makeRequest('/v1/get/'+_entryID,payload)
        if response_data: 
            if  response_data['success'] == 1:
                print(f"got data for entry {response_data['id']}, wanted data for entry {_entryID}")
                return response_data
            else:
                print(f"did not get data for entry {_entryID}")
        return None
        