#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys,os
sys.path.append("../../apstools")
sys.path.append("../../emiltools")
from emiltools.ACarchive import ACarchive

from emiltools.publisher import start_publisher, start_proxy
import multiprocessing
from multiprocessing import Process
from bluesky.callbacks.zmq import Publisher
import time

from bluesky import RunEngine
from bluesky.callbacks.best_effort import BestEffortCallback
from bluesky.utils import ProgressBarManager
from databroker.v2 import temp

from ophyd.sim import det1, det2  # two simulated detectors
from bluesky.plans import count

"""
Initialize Runner and databroker
"""
RE = RunEngine({})
bec = BestEffortCallback()
RE.subscribe(bec)
RE.waiting_hook = ProgressBarManager()


db = temp()
print(type(db))
# Insert all metadata/data captured into db.
RE.subscribe(db.v1.insert)  

"""
Set up Proxy
"""
manager = multiprocessing.Manager()
return_dict = manager.dict()
p_proxy = Process(target=start_proxy, args=[return_dict])
p_proxy.daemon = True
p_proxy.start()
print(return_dict)

time.sleep(1) # We need to wait a little time for the proxy to start
"""
Set up Publisher using NXWriterBessy
"""
#Start the publisher
file_path = os.getcwd()
print(file_path)
p_publisher = Process(target=start_publisher,args = [return_dict['out_port'],file_path])
p_publisher.daemon = True
p_publisher.start()
time.sleep(4) # We need to wait a little time for the process to start

# Subscribe our callback
publisher = Publisher('localhost:'+str(return_dict['in_port']))
pub_sub_id=RE.subscribe(publisher)

"""
Run Bluesky Experiments
"""
dets = [det1, det2]   # a list of any number of detectors
n=10
RE(count(dets,num=n,delay=[1 for i in range(n-1)]),Yield=0.5,Preparator='Micha',Comment="This is a highlevelcomment",DateofPreparation='15.02.2022',Elements="N,O,C", reason='Testing', Export='AC', Stuff='stuff')


"""
Test   AC archive, especially pushing hdf file
"""
file_name = 'nocheintest.hdf'
session = ACarchive()

session.token = '1'
entryID = session.createEntry({"Preparator":"Micha"},_entrytype='sample')

hdffiles= []
for path, subdirs, files in os.walk('.'):
    for name in files:
        if name.endswith(".hdf") or name.endswith(".h5"):
            hdffiles.append(os.path.join(path, name))
newest = max(hdffiles , key = os.path.getctime)
print(newest)
docid = session.uploadNexusFileBase64(entryID,newest,file_name)
session.downloadFile(entryID, docid,file_name)
session.editEntry(entryID,{'Comment':'This is a comment'})
data = session.getEntry(entryID)
print(data)

import h5py
assert h5py.is_hdf5(file_name)

session.deleteEntry(entryID)
