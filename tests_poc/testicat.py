#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 22 13:23:35 2022

@author: micha
"""

from __future__ import print_function
import icat
import icat.client
import icat.config
from icat.query import Query

icat.config.defaultsection = 'default'
config = icat.config.Config()
print(config)

config.add_variable("investigation", ("-in", "--investigation"),
                    dict(help="investiagtion name defined in config file"),
                    default="")

client, conf = config.getconfig()


client.login(conf.auth, conf.credentials)

print(client.sessionId)
client2, conf = config.getconfig()
client2.sessionId = client.sessionId



print("Login to %s was successful." % (conf.url))
print("User: %s" % (client.getUserName()))


investigation = client.assertedSearch(Query(client, "Investigation", conditions={"name": "= '"+conf.investigation+"'"}))[0]
print(f"Investigation {investigation}")

try:
    dataset = client.new("dataset")
    dataset.investigation = investigation
    dataset.type = client.assertedSearch(Query(client, "DatasetType", conditions={"name": "= 'other'"}))[0]
    dataset.name = "greetings"
    dataset.complete = False
    print(f"Dataset {dataset}")
    dataset.create()
except  icat.exception.ICATObjectExistsError:
    dataset = client.search(Query(client,"Dataset", conditions={"name": "= 'greetings'"}))[0]

path = "/home/micha/Documents/Beruf/Helmholtz/scripts-for-acarchive/libraries/emiltools/emiltools/2022_02_24/nx/00001_d3c37f0.hdf"
fname = "00001_d3c37f0.hdf"
df_format = client.assertedSearch(Query(client, "DatafileFormat", conditions={"name": "= 'NeXus'"}))[0]

datafile = client.new("datafile", name=fname, dataset=dataset, datafileFormat=df_format)
client.putData(path, datafile)