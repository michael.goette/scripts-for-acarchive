# Scripts for ACArchive

Contains scripts which can be used to publish measurement data from CatLab experiments to the AC archive. Scripts are adapted to the experiments.